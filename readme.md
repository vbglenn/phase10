# Starting the server
## Create docker image and start
We are going to start a default ruby image.
And start it with a bash shell while routing its 4567 port to your localhost 4567 port.
```
output=$( sudo docker create -ti -p 127.0.0.1:4567:4567 ruby bash )
sudo docker start $output
sudo docker attach $output
```

## Clone code install and run
Clone the code from the public repo.
Install the Gem dependencies.
And run the server.
```
git clone https://gitlab.com/vbglenn/phase10.git
cd phase10
bundle
ruby server.rb
```


# Playing the game
**GET /game**
The currently running game instance and its data.

**POST /game/join**

REQUEST
```
{
    "name": "A Username"
}
```

**POST /game/start**
Start the game

**POST /game/pull**
Pull a card from one of the decks.
This should be your first action in a turn.
Stack: opened, closed
opened => Throw away stack
closed => Default card stack

REQUEST
```
{
    "name": "A Username",
    "stack": "opened"
}
```

**POST /game/push**
Push a card from your hand to the Throw away stack.
This ends your turn.

REQUEST
```
{
    "name": "A Username",
    "card": 12
}
```

**POST /game/play_phase**
When you have the right cards for a phase you can play it.
Correct combinations:
AAA / BBB
You can also throw away cards to other existing stacks if you want to.

REQUEST
```
{
    "name": "A Username",
    "decks": [
        [4,4,4],
        [5,5,5]
    ],
    "other": [
        {
            "deckId": "guid",
            "cards": [
                12
            ]
        },
        {
            "deckId": "guid",
            "cards": [
                6
            ]
        }
    ]
}
```