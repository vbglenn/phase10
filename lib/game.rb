require 'json'

class Game
  attr_reader :closedDeck
  attr_reader :openDeck
  attr_reader :decks
  attr_reader :rounds
  attr_reader :state
  attr_reader :players

  def initialize
    @players = []
    generate_stacks

    @openDeck = []
    @openDeck.push(@closedDeck.pop)

    @decks = []
    @rounds = []
    @state = :open
  end

  def start
    raise 'Already started' if @state != :open
    raise 'No players' if @players.count == 0
    @rounds << Round.new(@players)
    @state = :playing
    self.to_json
  end

  def add_player name
    return nil if state != :open
    return get_player_by_name(name) if player_exists?(name)

    player = Player.new name
    player.deal(@closedDeck.pop(10))
    @players.push player
    return player
  end

  def pull(name, deckName)
    deck = get_deck_by_name(deckName)
    get_player_by_name(name).turn.take(deck)
  end

  def push name, cardValue
    player = get_player_by_name(name)
    player.turn.play(cardValue, @openDeck)
    end_turn
  end

  def play_phase name, decks
    player = get_player_by_name(name)
    played_decks = player.turn.play_phase(decks)
    played_decks.each {|deck| @decks.push(deck)}
  end

  def play_other name, other
    player = get_player_by_name(name)
    player.turn.play_other(@decks, other)
  end

  def to_json

    return {
        players: @players.map(&:to_h),
        openDeck: {
            count: @openDeck.count,
            top: @openDeck.first.to_h,
            deck: @openDeck.map(&:to_h)
        },
        closedDeck: {
            count: @closedDeck.count,
            deck: @closedDeck.map(&:to_h)
        },
        decks: @decks.map(&:to_h),
        rounds: @rounds.map(&:to_h),
        state: @state,
        currentRound: @rounds.count
    }.to_json
  end

  def get_player_by_name(name)
    return @players.select { |u| u.name == name }.first
  end

  private
  def end_turn
    if (@rounds.last.finished?)
      @rounds << Round.new(@players)
    else
      @rounds.last.next_player
    end
  end

  def generate_stacks
    @closedDeck = []
    (1..12).each do |value|
      8.times do
        @closedDeck.push(Card.new(value))
      end
    end
    # @closedDeck.shuffle!
  end

  def player_exists?(name)
    return @players.map(&:name).include?(name)
  end

  def get_deck_by_name(deckName)
    case deckName.to_sym
      when :opened
        deck = @openDeck
      when :closed
        deck = @closedDeck
      else
        raise 'Invalid stack'
    end
    return deck
  end
end
