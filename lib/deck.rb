require 'securerandom'

class Deck
  attr_reader :cards
  attr_reader :guid

  def initialize cards
    @cards = cards
    @guid = SecureRandom.uuid
  end

  def valid?
    cards.map(&:value).uniq.count == 1
  end

  def push cards
    cards.each { |card| @cards.push(card) }
  end

  def to_h
    {
        guid: @guid,
        cards: cards.map(&:to_h)
    }
  end
end