require_relative 'card'
require 'json'

class Player
  attr_reader :name
  attr_reader :phase
  attr_reader :hand
  attr_accessor :turns

  def initialize(name)
    @name = name
    @phase = 1
    @hand = []
    @turns = []
  end

  def deal cards
    @hand = cards
  end

  def push cards
    cards = [*cards]
    cards.each do |card|
      @hand.push card
    end
  end

  def pop_card value
    card = @hand.select { |c| c.value.to_s == value.to_s }.first
    raise 'Invalid card' if card.nil?
    @hand.delete_at(@hand.find_index(card))
    return card
  end

  def has_cards values
    handValues = @hand.map(&:value)
    contains_all?(handValues, values.map(&:to_i))
  end

  def pop_cards values
    values.map { |v| pop_card(v) }
  end

  def valid_phase? decks
    decks.all? { |deck| (deck.cards.count >= 3) && (deck.cards.map(&:value).uniq.count == 1) } && decks.count == 2
  end

  def played_phase?
    @turns.any? { |t| t.phase_played }
  end

  def turn
    @turns.last
  end

  def to_h
    {
        name: @name,
        phase: phase,
        playedPhase: played_phase?,
        handCount: @hand.count,
        hand: @hand.map(&:to_h)
    }
  end

  private
  def contains_all? ary1, ary2
    ary2 = ary2.dup
    ary1.each do |e|
      if i = ary2.index(e) then
        ary2.delete_at(i)
      end
    end
    ary2.empty?
  end
end