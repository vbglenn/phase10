class Turn
  #STATE :waiting, :open, :taken, :played
  attr_reader :state
  attr_reader :player
  attr_reader :phase_played

  def initialize(player)
    @player = player
    @state = :waiting
    player.turns << self
    @phase_played = false
  end

  def set_open
    @state = :open
  end

  def take(deck)
    raise 'Not your turn' if @state == :waiting
    raise 'Already took a card' if @state == :taken
    raise 'Already played' if @state == :played
    card = deck.pop
    @player.push(card)
    @state = :taken
    return card
  end

  def play(cardValue, deck)
    raise 'You have to pull first' if @state == :open
    raise 'Not your turn' if @state == :waiting
    raise 'Already played' if @state == :played
    card = @player.pop_card(cardValue)
    deck.push(card)
    @state = :played
  end

  def play_phase(valuesForDecks)
    raise 'You have to pull first' if @state == :open
    raise 'Not your turn' if @state == :waiting
    raise 'Already played' if @state == :played
    raise 'Invalid cards' unless @player.has_cards(valuesForDecks.flatten)

    decks = valuesForDecks.map { |deckValues| Deck.new(@player.pop_cards(deckValues)) }
    unless @player.valid_phase?(decks)
      cards = decks.map(&:cards).flatten
      @player.push(cards)
      raise 'Invalid phase'
    end
    @phase_played = true
    return decks
  end

  def play_other(decks, otherValues)
    raise 'You have to pull first' if @state == :open
    raise 'Not your turn' if @state == :waiting
    raise 'Already played' if @state == :played
    raise 'Not played phase yet' unless @player.played_phase?
    raise 'Invalid cards' unless @player.has_cards(otherValues.map{|v| v['cards']}.flatten)

    otherValues.map do |deckWithValues|
      deckId = deckWithValues['deckId']
      values = deckWithValues['cards']
      deck = decks.select{|deck| deck.guid == deckId}.first
      cards = @player.pop_cards(values)
      deck.push(cards)
      unless deck.valid?
        @player.push(cards)
        raise 'Invalid deck'
      end
    end
  end
end