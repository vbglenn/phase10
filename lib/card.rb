require 'json'

class Card
  attr_reader :value

  def initialize value
    @value = value
  end

  def to_h
    return {
        value: @value
    }
  end
end