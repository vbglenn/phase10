class Round
  attr_reader :players

  def initialize(players)
    raise 'Not enough players for round' if (players.nil? || players.empty?)
    @players = players
    @turns = @players.map { |player| Turn.new(player) }
    @turns.first.set_open
  end

  def players_played
    @turns.select { |s| s.state == :played }.map { |s| s.player }
  end

  def current_player
    @turns.select { |s| [:open, :taken].include?(s.state) }.first.player
  end

  def next_player
    @turns.select { |s| s.state == :waiting }.first.set_open
  end

  def finished?
    players_played == @players
  end

  def to_h
    {
        played: players_played.map(&:name),
        current_player: current_player.name
    }
  end
end