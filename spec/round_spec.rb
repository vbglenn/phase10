require './lib/round'
require './lib/player'
require './lib/turn'

RSpec.describe Round do
  context 'a new round' do
    before do
      @players = [
          Player.new('Jos'),
          Player.new('Frank')
      ]
    end

    it 'needs some players' do
      round = Round.new(@players)
      expect(round.players).to eq(@players)
    end

    it 'sets a start player' do
      round = Round.new(@players)
      expect(round.current_player).to eq(@players.first)
    end

    it 'has no played players' do
      round = Round.new(@players)
      expect(round.players_played).to be_empty
    end

    it 'crashes without players' do
      expect { Round.new([]) }.to raise_error('Not enough players for round')
    end

    it 'creates a turn for each player' do
      Round.new(@players)
      expect(@players.first.turn).not_to be_nil
      expect(@players.first.turn.state).to eq(:open)

      expect(@players.last.turn).not_to be_nil
      expect(@players.last.turn.state).to eq(:waiting)
    end
  end

  context :to_h do
    before do
      @players = [
          Player.new('Jos'),
          Player.new('Frank')
      ]
    end

    it do
      round = Round.new(@players)
      expect(round.to_h).to eq({
                                   played: [],
                                   current_player: 'Jos'
                               })
    end
  end

  # context :play do
  #   before do
  #     @players = [
  #         Player.new('Jos'),
  #         Player.new('Frank')
  #     ]
  #     @round = Round.new(@players)
  #   end
  #
  #   it 'turns the round to the next player' do
  #     @round.play @players.first
  #     expect(@round.current_player).to eq(@players.last)
  #   end
  #
  #   context 'when its the last player for a round' do
  #     it 'finishes the round' do
  #
  #     end
  #   end
  # end
end
