require './lib/card'

RSpec.describe Card do
  context :value do
    it 'sets its value' do
      card = Card.new(5)
      expect(card.value).to eq(5)
    end
  end

  context :to_h do
    it 'returns the value' do
      card = Card.new(7)
      expected_result = {
          value: 7
      }
      expect(card.to_h).to eq(expected_result)
    end
  end


end
