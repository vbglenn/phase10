require './lib/player'
require './lib/deck'

RSpec.describe Player do
  context 'Creating a player' do
    it 'sets its name' do
      player = Player.new 'Joske'
      expect(player.name).to eq('Joske')
    end

    it 'sets its default phase' do
      player = Player.new nil
      expect(player.phase).to eq(1)
    end

    it 'doesnt have any cards' do
      player = Player.new nil
      expect(player.hand).to be_empty
    end
  end

  context :deal do
    before do
      @player = Player.new 'Jos'
    end

    it 'can get cards dealt' do
      cards = [Card.new(5), Card.new(2)]
      @player.deal(cards)
      hand = @player.hand
      expect(hand.count).to eq(2)

      expect(hand.map(&:value)).to include(5)
      expect(hand.map(&:value)).to include(2)
    end
  end

  context :push do
    before do
      @player = Player.new 'Jos'
      @player.deal([Card.new(5), Card.new(2)])
    end

    it 'adds single extra card' do
      extra_card = Card.new(8)
      @player.push(extra_card)

      hand = @player.hand
      expect(hand.count).to eq(3)
      expect(hand.last).to eq(extra_card)
    end

    it 'adds multiple extra cards' do
      extra_cards = [Card.new(8), Card.new(9)]
      @player.push(extra_cards)

      hand = @player.hand
      expect(hand.count).to eq(4)
      extra_cards.each do |card|
        expect(hand).to include(card)
      end
    end
  end

  context :to_h do
    it do
      @player = Player.new 'Frank'
      @player.deal [Card.new(2)]
      @player.push(Card.new(3))

      expect(@player.to_h).to eq({
                                     name: 'Frank',
                                     phase: 1,
                                     playedPhase: false,
                                     handCount: 2,
                                     hand: [
                                         {value: 2},
                                         {value: 3}
                                     ]
                                 })
    end
  end

  context :pop_card do
    before do
      @player = Player.new 'Frank'
      @player.deal [Card.new(2), Card.new(3)]
    end
    it 'has card' do
      card = @player.pop_card(2)
      expect(card.value).to eq(2)
      expect(@player.hand.count).to eq(1)
      expect(@player.hand.first.value).to eq(3)
    end

    it 'doesnt have the card' do
      expect { @player.pop_card(4) }.to raise_error 'Invalid card'
    end
  end

  context :has_cards do
    it do
      player = Player.new 'Jos'
      player.deal([Card.new(2), Card.new(3)])
      expect(player.has_cards([2])).to be_truthy
      expect(player.has_cards([3])).to be_truthy
      expect(player.has_cards([2, 3])).to be_truthy
      expect(player.has_cards([3, 2])).to be_truthy
    end

    it 'with duplicates' do
      player = Player.new 'Jos'
      player.deal([Card.new(2), Card.new(3), Card.new(3)])
      expect(player.has_cards([3, 3])).to be_truthy
      expect(player.has_cards([3, 2])).to be_truthy
      expect(player.has_cards([3, 2, 2])).to be_falsy
      expect(player.has_cards([3, 3, 2])).to be_truthy
    end

    it 'with strings' do
      player = Player.new 'Jos'
      player.deal([Card.new(2), Card.new(3)])
      expect(player.has_cards(['2'])).to be_truthy
      expect(player.has_cards(['3'])).to be_truthy
      expect(player.has_cards(['2', '3'])).to be_truthy
      expect(player.has_cards(['3', '2'])).to be_truthy
    end
  end

  context :valid_phase? do
    before do
      @player = Player.new 'Jos'
      @cards = [
          Card.new(2),
          Card.new(2),
          Card.new(2),
          Card.new(3),
          Card.new(3),
          Card.new(3),
          Card.new(4),
          Card.new(4),
          Card.new(5),
      ]
      @player.deal(@cards)
    end

    it 'only one correct phase' do
      decks = [
          Deck.new([Card.new(2),
                    Card.new(2),
                    Card.new(2)])
      ]
      expect(@player.valid_phase?(decks)).to be_falsy
    end

    it 'valid phase' do
      decks = [
          Deck.new([Card.new(2),
                    Card.new(2),
                    Card.new(2)]),
          Deck.new([Card.new(3),
                    Card.new(3),
                    Card.new(3)])
      ]
      expect(@player.valid_phase?(decks)).to be_truthy
    end
  end
end
