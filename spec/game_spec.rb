require './lib/game'
require './lib/player'
require './lib/card'
require './lib/round'
require './lib/turn'

describe Game do
  context 'a new game' do
    before { @game = Game.new }
    it 'creates new closed deck' do
      expect(@game.closedDeck.count).to eq(95)
      groupedCards = @game.closedDeck.group_by { |c| c.value }
      expect(groupedCards.count).to eq(12)
      expect(groupedCards.keys.sort).to eq([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
      expect(groupedCards.map { |k, v| v.count }.sort).to eq([7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8])

      #todo Shuffles the deck
    end

    it 'creates new open deck' do
      expect(@game.openDeck.count).to eq(1)
      card = @game.openDeck.first.value
      cardsInClosedDeck = @game.closedDeck.select { |c| c.value == card }
      expect(cardsInClosedDeck.count).to eq(7)
    end

    it 'creates an empty playingField' do
      expect(@game.decks.count).to eq(0)
    end

    it 'sets rounds' do
      expect(@game.rounds.count).to eq(0)
    end

    it 'has open state' do
      expect(@game.state).to eq(:open)
    end
  end

  context :start do
    before do
      @game = Game.new
      @game.add_player('Joske')
      @game.start
    end

    it 'state is playing' do
      expect(@game.state).to eq(:playing)
    end

    it 'not open game' do
      expect { @game.start }.to raise_error('Already started')
    end

    it 'Without players' do
      game = Game.new
      expect { game.start }.to raise_error('No players')
    end

    it 'returns self' do
      game = Game.new
      game.add_player('Joske')
      new_game = game.start
      expect(new_game).to eq(game.to_json)
    end
  end

  context :add_player do
    before do
      @game = Game.new
    end

    it 'adds the player' do
      @game.add_player 'Joske'
      expect(@game.players.count).to eq(1)
      expect(@game.players.first.name).to eq('Joske')
    end

    it 'deals the player cards from the closed Deck' do
      player = @game.add_player 'Someone'
      expect(player.hand.count).to eq(10)
      expect(@game.closedDeck.count).to eq(85)

      player = @game.add_player 'Another one'
      expect(player.hand.count).to eq(10)
      expect(@game.closedDeck.count).to eq(75)
    end

    context 'adding already existing user' do
      it 'doesnt add new one' do
        @game.add_player 'Frank'
        @game.add_player 'Frank'
        expect(@game.players.count).to eq(1)
      end

      it 'returns the already created user' do
        user = @game.add_player 'Frank'
        new_user = @game.add_player 'Frank'
        expect(user).to eq(new_user)
      end
    end

    context 'when game is started' do
      before do
        @game.add_player 'Jos'
        @game.start
      end
      it 'does nothing' do
        @game.add_player 'Frank'
        expect(@game.players.count).to eq(1)
      end
    end
  end

  context :pull do
    before do
      @game = Game.new
      @frank = @game.add_player('Frank')
      @game.add_player('Jos')
      @game.start
    end

    context 'when its your turn' do
      it 'closed deck' do
        @game.pull 'Frank', :closed
        expect(@game.closedDeck.count).to eq(74)
        expect(@game.openDeck.count).to eq(1)
        expect(@game.get_player_by_name('Frank').hand.count).to eq(11)
      end

      it 'open deck' do
        @game.pull 'Frank', :opened
        expect(@game.closedDeck.count).to eq(75)
        expect(@game.openDeck.count).to eq(0)
        expect(@game.get_player_by_name('Frank').hand.count).to eq(11)
      end

      it 'invalid deck' do
        expect { @game.pull 'Frank', :another }.to raise_error('Invalid stack')
      end
    end

    context 'when its not your turn' do
      it do
        expect { @game.pull 'Jos', :closed }.to raise_error 'Not your turn'
      end
    end
  end

  context :push do
    before do
      @game = Game.new
      @frank = @game.add_player('Frank')
      @jos = @game.add_player('Jos')
      @game.start
    end

    context 'when its your turn' do
      before do
        value = @frank.hand.first.value
        @game.pull 'Frank', :closed
        @game.push 'Frank', value
      end

      it 'updates the round' do
        expect(@game.rounds.last.players_played.count).to eq(1)
      end

      it 'adds the card to the openDeck' do
        expect(@game.openDeck.count).to eq(2)
      end

      it 'removes the card from the player' do
        expect(@frank.hand.count).to eq(10)
      end
    end

    context 'when its not your turn' do
      it do
        value = @jos.hand.first.value
        expect { @game.push 'Jos', value }.to raise_error 'Not your turn'
      end
    end
  end
end
