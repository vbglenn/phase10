require './lib/game'
require './lib/player'
require './lib/card'
require './lib/round'
require './lib/turn'
require './lib/deck'
require 'json'
require 'sinatra'
require 'rack'
require 'rack/contrib'
require 'pry'

use Rack::PostBodyContentTypeParser
set :bind, '0.0.0.0'

game = Game.new

get '/' do
  "Phase 10 Game \n Call /game to start"
end

get '/game' do
  game.to_json
end

post '/game/start' do
  game.start
end

post '/game/join' do
  name = params['name']
  player = game.add_player name
  player.to_h.to_json
end

post '/game/pull' do
  name = params['name']
  stack = params['stack']
  card = game.pull name, stack
  return card.to_h.to_json
end

post '/game/push' do
  puts params
  name = params['name']
  card = params['card']
  game.push(name,card)
  ''
end

post '/game/play_phase' do
  name = params['name']
  decks = params['decks']
  other = params['other']
  game.play_phase(name,decks) unless decks.nil?
  game.play_other(name, other) unless other.nil?
  ''
end